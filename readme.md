# corestack-wrapper #

 Branch | Build Status
 :--- | :---
```master```| [![Logo][master-status]](https://ci.appveyor.com/project/thebrianlopez/corestack-deploy/branch/master)
```dev```| [![Logo][dev-status]](https://ci.appveyor.com/project/thebrianlopez/corestack-deploy/branch/dev)

This is a wrapper project that references the entire corestack repo as a sub module. This is an attempt to separate the corestack build process away from the source code to allow build/deploy automation to evolve separately from the app source code.

## Instructions ##
1. Clone repo
```
git clone <repo url>
```
1. Initialize submodules
```
git submodule update --init --recursive
```
1. Build app
```
./build.sh
```
or
```
build.cmd
```

[Documentation]: https://axsteam.atlassian.net/wiki/display/OPS/Operations+Home
[dev-status]: https://ci.appveyor.com/api/projects/status/qk7fkyubvjr5w8pd/branch/dev?svg=true
[master-status]: https://ci.appveyor.com/api/projects/status/qk7fkyubvjr5w8pd/branch/master?svg=true