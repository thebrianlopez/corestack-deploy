#!/bin/bash

git submodule update --init --recursive

# This command goes through each .cspyoj file and changes the output path to the solution folder. This way all source code is in the same folder for easy debugging
rpl -R -v -x '.csproj' '<OutputPath>bin\Debug\</OutputPath>' '<OutputPath>$(SolutionDir)\..\build\Debug\$(AssemblyName)</OutputPath>' .
rpl -R -v -x '.csproj' '<OutputPath>bin\Release\</OutputPath>' '<OutputPath>$(SolutionDir)\..\build\Release\$(AssemblyName)</OutputPath>' .
