::@echo off

"%~dp0..\.nuget\NuGet.exe" "sources" "disable" "-name" "https://www.nuget.org/api/v2/" "-configfile" "%~dp0..\.nuget\NuGet.Config" -verbosity detailed
"%~dp0..\.nuget\NuGet.exe" "sources" "disable" "-name" "Microsoft Visual Studio Offline Packages" "-configfile" "%~dp0..\.nuget\NuGet.Config" -verbosity detailed
:: "%~dp0..\.nuget\NuGet.exe" "sources" "enable" "-name" "thebrianlopez" "-configfile" "%~dp0..\.nuget\NuGet.Config" -verbosity detailed
"%~dp0..\.nuget\NuGet.exe" "sources" "list" "-configfile" "%~dp0..\.nuget\NuGet.Config" -verbosity detailed


IF NOT EXIST "%~dp0..\src\packages\rpl" (
	echo "Restoring packages"
	"%~dp0..\.nuget\NuGet.exe" "Install" "rpl" "-OutputDirectory" "%~dp0..\src\packages" "-ExcludeVersion" "-configfile" "%~dp0..\.nuget\NuGet.Config" -verbosity detailed
)
IF NOT EXIST "%~dp0..\src\packages\octotools" (
	echo "Restoring packages"
	"%~dp0..\.nuget\NuGet.exe" "Install" "octotools" "-OutputDirectory" "%~dp0..\src\packages" "-ExcludeVersion" "-configfile" "%~dp0..\.nuget\NuGet.Config" -verbosity detailed
)

"%~dp0..\src\packages\rpl\tools\bin\rpl.exe" -R -x .csproj "<OutputPath>bin\Debug\</OutputPath>" "<OutputPath>$(SolutionDir)\..\build\Debug\$(AssemblyName)</OutputPath>" .
"%~dp0..\src\packages\rpl\tools\bin\rpl.exe" -R -x .csproj "<OutputPath>bin\Release\</OutputPath>" "<OutputPath>$(SolutionDir)\..\build\Release\$(AssemblyName)</OutputPath>" .
"%~dp0..\src\packages\rpl\tools\bin\rpl.exe" -R -x .csproj "<OutputPath>bin\</OutputPath>" "<OutputPath>$(SolutionDir)\..\build\Release\$(AssemblyName)</OutputPath>" .

::powershell -exec Unrestricted -c "Import-module axsadministration; msbuild src\BuildAll.sln"
"C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe" "src\BuildAll.sln"

:: git status
:: git remote -v 
:: git log -3
::git describe

echo %errorlevel%